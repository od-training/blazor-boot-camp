﻿using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace CommonServiceInterfaces
{
  public class Employee
  {
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public int HoursWorked { get; set; }
    public int HourlyWage { get; set; }
  }

  public interface IEmployeeLoader
  {
    Task<ICollection<Employee>> GetEmployees();
    Task<ICollection<Employee>> GetEmployees(Dictionary<string, string> queryDictionary = null);
    public Task<Employee> GetEmployee(string id);
  }

  public abstract class BaseEmployeeLoader: IEmployeeLoader
  {
    protected readonly JsonSerializerOptions jsonSerializerOptions;
    protected readonly HttpClient client;

    public BaseEmployeeLoader(){}

    public BaseEmployeeLoader(IHttpClientFactory clientFactory)
    {
      client = clientFactory.CreateClient();
      jsonSerializerOptions = new JsonSerializerOptions
      {
        PropertyNamingPolicy = JsonNamingPolicy.CamelCase
      };
    }

    public abstract Task<Employee> GetEmployee(string id);
    public abstract Task<ICollection<Employee>> GetEmployees();
    public abstract Task<ICollection<Employee>> GetEmployees(Dictionary<string, string> queryDictionary = null);
  }


}