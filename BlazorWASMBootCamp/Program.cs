using System;
using System.Net.Http;
using System.Threading.Tasks;
using BlazorWASMBootCamp.Data;
using BootCampSteps;
using CommonServiceInterfaces;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace BlazorWASMBootCamp
{
  public static class Program
  {
    public static async Task Main(string[] args)
    {
      var builder = WebAssemblyHostBuilder.CreateDefault(args);
      builder.RootComponents.Add<App>("#app");

      builder.Services.AddScoped(sp => new HttpClient {BaseAddress = new Uri("https://api.angularbootcamp.com/")});
      builder.Services.AddScoped<IEmployeeLoader, EmployeeLoaderServiceWasm>();

      await builder.Build().RunAsync();
    }
  }
}