﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

using CommonServiceInterfaces;

using Microsoft.AspNetCore.WebUtilities;

namespace BlazorWASMBootCamp.Data
{
  public class EmployeeLoaderServiceWasm : IEmployeeLoader
  {
    private readonly string _uri = "employees";

    private readonly HttpClient _httpClient;
    public EmployeeLoaderServiceWasm(HttpClient httpClient)
    {
      _httpClient = httpClient;
    }
    public async Task<ICollection<Employee>> GetEmployees()
    {
      return await _httpClient.GetFromJsonAsync<Employee[]>(_uri);
    }

    public async Task<ICollection<Employee>> GetEmployees(Dictionary<string, string> queryDictionary)
    {
      var uri = QueryHelpers.AddQueryString(_uri, queryDictionary);
      return await _httpClient.GetFromJsonAsync<Employee[]>(uri);
    }

    public async Task<Employee> GetEmployee(string Id)
    {
      return await _httpClient.GetFromJsonAsync<Employee>($"{_uri}/{Id}");
    }
  }
}