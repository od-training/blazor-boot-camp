﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.WebUtilities;

using System;
using System.Collections.Generic;
using System.Web;

//The following was derived from:
//  https://chrissainty.com/working-with-query-strings-in-blazor/

namespace BootCampSteps
{
  public static class NavigationManagerExtensions
  {
    public static T? TryGetQueryString<T>(this NavigationManager NavManager, string key)
    {
      var uri = NavManager.ToAbsoluteUri(NavManager.Uri);

      if (QueryHelpers.ParseQuery(uri.Query).TryGetValue(key, out var valueFromQueryString))
      {
        if (typeof(T) == typeof(int) && int.TryParse(valueFromQueryString, out var valueAsInt))
        {
          return (T)(object)valueAsInt;
        }

        if (typeof(T) == typeof(string))
        {
          return (T)(object)valueFromQueryString.ToString();
        }

        if (typeof(T) == typeof(decimal) && decimal.TryParse(valueFromQueryString, out var valueAsDecimal))
        {
          return (T)(object)valueAsDecimal;

        }
      }

      return default;
    }


    public static void ChangeUrl(this NavigationManager NavManager, string key, string value)
    {

      string url = RemoveQueryStringByKey(NavManager.Uri, key);

      NavManager.NavigateTo(QueryHelpers.AddQueryString(url, key, value));
    }

    private static string RemoveQueryStringByKey(string url, string key)
    {
      var uri = new Uri(url);

      // this gets all the query string key value pairs as a collection
      var newQueryString = HttpUtility.ParseQueryString(uri.Query);

      // this removes the key if exists
      newQueryString.Remove(key);

      // this gets the page path from root without QueryString
      string pagePathWithoutQueryString = uri.GetLeftPart(UriPartial.Path);

      return newQueryString.Count > 0
          ? string.Format("{0}?{1}", pagePathWithoutQueryString, newQueryString)
          : pagePathWithoutQueryString;
    }



  }
}