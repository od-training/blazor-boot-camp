﻿using System.Collections.Generic;

namespace BootCampSteps.Pages._208_EventCallback
{
  public record Item(int Quantity, string Description);

  public record Order(string Id, string Customer, List<Item> Items);
}