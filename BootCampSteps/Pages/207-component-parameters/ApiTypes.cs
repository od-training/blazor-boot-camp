﻿using System.Collections.Generic;

namespace BootCampSteps.Pages._207_component_parameters
{
  public record Item(int Quantity, string Description);

  public record Order(string Id, string Customer, List<Item> Items);
}