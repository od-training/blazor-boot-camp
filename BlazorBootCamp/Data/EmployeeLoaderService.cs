﻿using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

using CommonServiceInterfaces;

using Microsoft.AspNetCore.WebUtilities;

namespace BlazorBootCamp.Data
{
  public class EmployeeLoaderService : BaseEmployeeLoader
  {
    private readonly string _uri = "https://api.angularbootcamp.com/employees";
    public EmployeeLoaderService(IHttpClientFactory clientFactory) : base(clientFactory) { }

    public async override Task<ICollection<Employee>> GetEmployees()
    {
      var requestMessage = new HttpRequestMessage(HttpMethod.Get, _uri); 

      var request = await client.SendAsync(requestMessage);

      if (request.IsSuccessStatusCode)
      {
        var responseStream = await request.Content.ReadAsStreamAsync();

        return await JsonSerializer.DeserializeAsync<ICollection<Employee>>(responseStream, jsonSerializerOptions);
      }
      else
      {
        return new List<Employee>();
      }
    }

    public async override Task<ICollection<Employee>> GetEmployees(Dictionary<string, string> queryDictionary)
    {
      var requestMessage = new HttpRequestMessage(HttpMethod.Get, QueryHelpers.AddQueryString(_uri, queryDictionary));

      var request = await client.SendAsync(requestMessage);

      if (request.IsSuccessStatusCode)
      {
        var responseStream = await request.Content.ReadAsStreamAsync();

        return await JsonSerializer.DeserializeAsync<ICollection<Employee>>(responseStream, jsonSerializerOptions);
      }
      else
      {
        return new List<Employee>();
      }
    }

    public async override Task<Employee> GetEmployee(string id)
    {
      var requestMessage = new HttpRequestMessage(HttpMethod.Get, $"https://api.angularbootcamp.com/employees/{id}");

      var request = await client.SendAsync(requestMessage);

      if (request.IsSuccessStatusCode)
      {
        var responseStream = await request.Content.ReadAsStreamAsync();

        return await JsonSerializer.DeserializeAsync<Employee>(responseStream, jsonSerializerOptions);
      }
      else
      {
        return null;
      }
    }
  }
}