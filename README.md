
Microsoft Docs
https://docs.microsoft.com/en-us/aspnet/core/blazor/?view=aspnetcore-5.0
https://docs.microsoft.com/en-us/aspnet/core/mvc/views/razor?view=aspnetcore-5.0

Data Binding
https://docs.microsoft.com/en-us/aspnet/core/blazor/components/data-binding?view=aspnetcore-6.0


Fast
https://www.fast.design/docs/integrations/blazor/
Fast/Fluent/Blazor
https://github.com/microsoft/fast-blazor/blob/main/examples/FluentUIServerSample/Pages/Webcomponents.razor
https://uifabricicons.azurewebsites.net/
https://developer.microsoft.com/en-us/fluentui#/styles/web/icons

Unit testing
https://www.lambdatest.com/blog/nunit-vs-xunit-vs-mstest/
https://bunit.dev/docs/getting-started/writing-tests.html?tabs=xunit
https://docs.microsoft.com/en-us/aspnet/core/blazor/test?view=aspnetcore-6.0

JS Interop
https://wellsb.com/csharp/aspnet/blazor-webcam-capture

Forms
https://docs.microsoft.com/en-us/dotnet/api/microsoft.aspnetcore.components.forms.dataannotationsvalidator?view=aspnetcore-6.0
https://docs.microsoft.com/en-us/dotnet/api/microsoft.aspnetcore.components.forms.editcontext?view=aspnetcore-6.0
https://docs.microsoft.com/en-us/aspnet/core/blazor/forms-validation?view=aspnetcore-6.0
https://stackoverflow.com/questions/3413715/how-to-create-custom-data-annotation-validators
https://github.com/dotnet/aspnetcore/issues/7573

Routing
https://docs.microsoft.com/en-us/aspnet/core/blazor/fundamentals/routing?view=aspnetcore-5.0#query-string-and-parse-parameters
https://stackoverflow.com/questions/56876631/setting-a-startup-page-in-blazor

Http
https://docs.microsoft.com/en-us/dotnet/api/system.net.http.httpclient?view=net-5.0

State Management
https://docs.microsoft.com/en-us/aspnet/core/blazor/state-management?view=aspnetcore-6.0&pivots=server#in-memory-state-container-service-server

Resources
https://www.youtube.com/playlist?list=PL8h4jt35t1wjvwFnvcB2LlYL4jLRzRmoz
https://blazor-university.com/

Libraries
https://github.com/mrpmorris/Fluxor

Key Names
Steve Sanderson
David Fowler
Dan Roth
Rob Eisenberg

Videos
https://www.youtube.com/watch?v=kLhoRyLxwAE